package cromosoma;



public class cromosomaFuncionGenerica extends cromosoma{
	
	// extremos del intervalo considerado
	// para los valores del dominio		
	int[] longitudCromosoma;
	int longitudTotal;
	int numGenes=1;
	double xMaxf1=250;
	double xMinf1=-250;
	double xMaxf2=6;
	double xMinf2=-6;	
	double xMaxf3=12.1;
	double xMinf3=-3;
	double yMaxf3=5.8;
	double yMinf3=4.1;
	double xMaxf5=10;
	double xMinf5=-10;
	double xMaxf4=Math.PI;
	double xMinf4=0;
	

	
	
	public double getxMax(int gen){
		double xMax=0;
		
		switch (tipoFuncion){
		case 1:xMax=xMaxf1;
			break;
		case 2:
			  xMax = xMaxf2;	
			break;
		case 4: xMax =xMaxf4;
			break;
		case 5:xMax =xMaxf5;
			
			break;
		case 3:
				if (gen==0)
					xMax =xMaxf3;
				else
					xMax =yMaxf3;
						
			break;
		default:break;
		}
		return xMax;
	}
	
	public double getxMin(int gen){
		double xMin=0;
		
		switch (tipoFuncion){
		case 1:xMin=xMinf1;
			break;
		case 2:
			xMin = xMinf2;	
			break;
		case 4: xMin =xMinf4;
			break;
		case 5:xMin =xMinf5;
			
			break;
		case 3:
				if (gen==0)
					xMin =xMinf3;
				else
					xMin =yMinf3;
						
			break;
		default:break;
		}
		return xMin;
	}
	
	public cromosomaFuncionGenerica(int tipoFuncion,int n){		
		this.aptitud=0;
		this.tipoFuncion=tipoFuncion;
		if (tipoFuncion==2 || tipoFuncion==3 || tipoFuncion==5)
			this.numGenes=2;
		else if(tipoFuncion==4)
			this.numGenes=n;
		else if (tipoFuncion==1)
			this.numGenes=1;			
	}
	
	
	
	public cromosomaFuncionGenerica() {
		// TODO Auto-generated constructor stub
	}
	public cromosomaFuncionGenerica(cromosomaFuncionGenerica hijo1) {
		this.aptitud = hijo1.getAptitud();
		this.fenotipo=hijo1.getFenotipo();
		this.tipoFuncion=hijo1.getTipoFuncion();
		int n = hijo1.getNumGenes();
		for (int i=0; i<n;i++){
			longitudCromosoma=hijo1.getLongitudCromosoma();
		}				
		this.punt_acum=hijo1.punt_acum;		
		this.numGenes=hijo1.numGenes;
		this.longitudTotal=hijo1.getLongitudTotal();
		copiaGenes(hijo1);
		
		
	}	
	
	private int[] getLongitudCromosoma() {
		return longitudCromosoma;
		
	}


	private void copiaGenes(cromosomaFuncionGenerica hijo1){
		
		int cantidadGen=0;
		setGenes(new boolean[hijo1.getLongitudTotal()]);
		for (int i=0;i<numGenes;i++){
			for (int j=0;j<longitudCromosoma[i];j++){
				this.getGenes()[cantidadGen]=hijo1.getGenes(cantidadGen);
				cantidadGen++;
			}
		}
	}

	


	private void fenotipo() {//el valor del individuo
		
		int[] bin = bin_dec(getGenes(), longitudCromosoma);		
		
		double valores[] = new double[numGenes];
		
		switch (tipoFuncion){
		case 1:
			valores[0]= xMinf1 + (xMaxf1 - xMinf1) * bin[0]/ (Math.pow(2,longitudCromosoma[0]) - 1);
			break;
		case 2:
			valores[0]= xMinf2 + (xMaxf2 - xMinf2) * bin[0]/ (Math.pow(2,longitudCromosoma[0]) - 1) ;
			valores[1]= xMinf2 + (xMaxf2 - xMinf2) * bin[1]/ (Math.pow(2,longitudCromosoma[1]) - 1) ;
			break;
		
		case 3:
			valores[0]= xMinf3 + (xMaxf3 - xMinf3) * bin[0]/ (Math.pow(2,longitudCromosoma[0]) - 1) ;
			valores[1]= yMinf3 + (yMaxf3 - yMinf3) * bin[1]/ (Math.pow(2,longitudCromosoma[1]) - 1) ;
			break;
		
		case 4:
			for (int i=0;i<numGenes;i++)
				valores[i]=  xMinf4 + (xMaxf4 - xMinf4) * bin[i]/ (Math.pow(2,longitudCromosoma[i]) - 1);		
			break;
		case 5:
			valores[0]= xMinf5 + (xMaxf5 - xMinf5) * bin[0]/ (Math.pow(2,longitudCromosoma[0]) - 1);
			valores[1]= xMinf5 + (xMaxf5 - xMinf5) * bin[1]/ (Math.pow(2,longitudCromosoma[0]) - 1);
			break;
		default: break;
	}
				
		fenotipo = valores;
		
		
	} 
	
	public double evalua() {//adaptacion del individuo
		
		fenotipo();
		aptitud =  f(fenotipo);
		return aptitud;
		
	}
	
	private double f(double[] fenotipo) {
		double resultado=0;
		switch (tipoFuncion){
			case 1:				
				 resultado = -Math.abs(fenotipo[0]*Math.sin(Math.sqrt(Math.abs(fenotipo[0]))));
				break;
			case 2:
				resultado =  (2186 - Math.pow ((Math.pow(fenotipo[0], 2) + fenotipo[1] - 11),2) - Math.pow( (fenotipo[0] + Math.pow(fenotipo[1],2) - 7),2) )/2186;
				break;
			
			case 3:
				resultado = 21.5 + fenotipo[0]*Math.sin(4*Math.PI*fenotipo[0]) + fenotipo[1]*Math.sin(20*Math.PI*fenotipo[1]);
				break;
			
			case 4:
				
				for (int i=1;i<=numGenes;i++){
					//resultado += Math.sin(fenotipo[i])*Math.pow(  ( (i+1)* Math.pow(fenotipo[i], 2)  )/Math.PI     , 20);
					double xi = ((i+1)*fenotipo[i-1])/Math.PI; 					
					resultado+= Math.sin(fenotipo[i-1])* Math.pow(Math.sin(xi), 20);
				}
				resultado = resultado *-1;
				break;
			case 5:
				double resultado1=0;
				double resultado2=0;
				for (int i=1;i<=5;i++){
					resultado1 += i * Math.cos( (i+1)*fenotipo[0]+i );
				}
				for (int i=1;i<=5;i++){
					resultado2 += i * Math.cos( (i+1)*fenotipo[1]+i );
				}
				resultado = resultado1*resultado2;
				
				break;
			default: break;
		}
		/*float val = 37.777779;

		float rounded_down = floorf(val * 100) / 100;  
		float nearest = roundf(val * 100) / 100; 
		float rounded_up = ceilf(val * 100) / 100;  */
		/*DecimalFormat df = new DecimalFormat("###.#####");
		String res = df.format(resultado);
		return Double.parseDouble(res);*/
		

		
		return Math.ceil(resultado*1000)/1000;
	}
	
	private int calculaLongitudCromosoma (double Xmax, double Xmin, double tol){
		
		int longitud;		
		longitud = (int) Math.round(Math.log(1+ (Math.abs(Xmax-Xmin)/tol)) / Math.log(2));		
		return longitud;
		
	}

	private int[] bin_dec(boolean[] genes, int[] longitudCromosoma2) {
		
		String binaryGen = "";
		int [] resultadoFinal = new int[numGenes];
		int cantidadGenes=0;
		for (int i=0;i<numGenes;i++){
			for (int j=0; j<longitudCromosoma2[i];j++){
				if (genes[cantidadGenes])
					binaryGen = binaryGen+'1' ;			
				else
					binaryGen = binaryGen+'0';	
				cantidadGenes++;
			}
			resultadoFinal[i]=Integer.parseInt(binaryGen, 2);	
			binaryGen = "";			
		}
	
		return resultadoFinal;
	}
	
	/*private int binarioADecimal(String numeroBinario){
		  
		  int longitud = numeroBinario.length();//Numero de digitos que tiene nuestro binario
		  int resultado = 0;//Aqui almacenaremos nuestra respuesta final
		  int potencia = longitud - 1;
		  for(int i = 0;i < longitud;i++){//recorremos la cadena de numeros
		   if(numeroBinario.charAt(i) == '1'){
		    resultado += Math.pow(2,potencia);
		   }
		   potencia --;//drecremantamos la potencia
		  }
		  return resultado;
		 }*/
	
	

	public void inicializaCromosoma(double tol) {
		
		longitudCromosoma = new int [numGenes];
		int cantidadGenes =0;
		if (tipoFuncion==1){
			longitudCromosoma[0]= calculaLongitudCromosoma(xMaxf1,xMinf1,tol);
			cantidadGenes=longitudCromosoma[0];
		}
		if (tipoFuncion==2){
			longitudCromosoma[0] = calculaLongitudCromosoma(xMaxf2,xMinf2,tol);
			longitudCromosoma[1] = calculaLongitudCromosoma(xMaxf2,xMinf2,tol);
			cantidadGenes=longitudCromosoma[0]+longitudCromosoma[1];
		}
		if (tipoFuncion==3){
			longitudCromosoma[0] = calculaLongitudCromosoma(xMaxf3,xMinf3,tol);
			longitudCromosoma[1] = calculaLongitudCromosoma(yMaxf3,yMinf3,tol);
			cantidadGenes=longitudCromosoma[0]+longitudCromosoma[1];
		}
		if (tipoFuncion==5){
			longitudCromosoma[0] = calculaLongitudCromosoma(xMaxf5,xMinf5,tol);
			longitudCromosoma[1] = calculaLongitudCromosoma(xMaxf5,xMinf5,tol);
			cantidadGenes=longitudCromosoma[0]+longitudCromosoma[1];
		}
		if (tipoFuncion==4){
			
			for (int i=0;i<numGenes;i++){
				longitudCromosoma[i] = calculaLongitudCromosoma(xMaxf4,xMinf4,tol);
				cantidadGenes+=longitudCromosoma[i];
			}
			
		}
		this.longitudTotal=cantidadGenes;
		
		setGenes(new boolean[cantidadGenes]);
		
		for (int i = 0; i <cantidadGenes; i++) {
			
			double prob = Math.random();
			if (prob>=0.5)
				getGenes()[i]=true;
			else
				getGenes()[i]=false;
		}
	}
	
	public double getX0(){		
		return fenotipo[0];
	}
	public double getY(){
		return fenotipo[1];
	}
	public double getX1(){
		return fenotipo[2];
	}
	public double getX2(){
		return fenotipo[3];
	}
	public double getX3(){
		return fenotipo[4];
	}
	public double getX4(){
		return fenotipo[5];
	}
	public double getX5(){
		return fenotipo[6];
	}
	
	public int getLongitudTotal() {
		return longitudTotal;
	}


	public void setLongitudTotal(int longitudTotal) {
		this.longitudTotal = longitudTotal;
	}


	public void setNumGenes(int numGenes) {
		this.numGenes = numGenes;
	}
	
	public void setGen(int i, String binario){
		int longitudDeGen = this.longitudCromosoma[i];
		int offset=0;
		int j=0;
		
		while(j!=i){
			offset = offset+longitudCromosoma[j];
			j++;
		}
		//El offset ya sabe donde debo empezar a insertar
		
		int longBin=binario.length();
		while (longBin>0){
			int c=binario.charAt(longBin-1);
			if (c=='1'){
				getGenes()[offset+longitudDeGen -1]=true;
			}
			else{
				getGenes()[offset+longitudDeGen -1]=false;
			}
			offset--;
			longBin--;
		}
		
		while (offset>0){
			getGenes()[offset+longitudDeGen -1]=false;
			offset--;
		}
		
	}




	
	
	public int getNumGenes() {
		return numGenes;
	}

	

	public int getLongitudCromosoma(int i) {
		return longitudCromosoma[i];
	}

	
	
	public String toString(){
		String resultado = "";
		for (int i=0;i<longitudTotal;i++)
			resultado += getGenes()[i] +  " , ";
		return "Genes: " + resultado + " fenotipo: " + fenotipo + " Aptitud: " + aptitud + " puntuación: " + puntuacion + " Punt acumulada: " + punt_acum +'\n';
		
	}

}
package algoritmoGenetico;


public enum Funciones {
	Funcion1(1){},
	Funcion2(2){},
	Funcion3(3){},
	Funcion4(4){},
	Funcion5(5){};
	
	private int n;

	private Funciones(int d){
		this.n = d;
	}
	
	public int getNum(){
		return n;
	}
	
}

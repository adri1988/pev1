package gui;


import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.math.plot.Plot2DPanel;

public class PanelGrafica extends JPanel implements Observador{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Controlador control;
	private JLabel etFuncion;
	private JLabel panelError;

	public PanelGrafica(Controlador c){
		control = c;
		control.addObserver(this);
		init();
	}
	
	private void init(){
		this.setBorder(BorderFactory.createTitledBorder("Gr�fica"));
		setLayout(new BorderLayout());
		etFuncion = new JLabel();
		
		panelError=new JLabel("Datos no validos. Vuelva a probar.");
		add(panelError, BorderLayout.CENTER);
		panelError.setVisible(false);
		panelError.setHorizontalAlignment(SwingConstants.CENTER);
		panelError.setFont(new Font("Serief", Font.BOLD, 48));
		
		/*
		JTabbedPane tabbedPane = new JTabbedPane();

		JComponent panel1 = makeTextPanel("grafica AG #1");
		
		tabbedPane.addTab("AG 1",null, panel1,
		                  "AG 1");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		JComponent panel2 = makeTextPanel("Panel #2");
		tabbedPane.addTab("AG 2", null, panel2,
		                  "AG 2");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

		JComponent panel3 = makeTextPanel("Panel #3");
		tabbedPane.addTab("AG 3", null, panel3, "AG 3");
		tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		
		tabbedPane.addTab("AG 3.5", null, panel3, "AG 3.5");

		JComponent panel4 = makeTextPanel(
		        "Panel #4 (has a preferred size of 410 x 50).");
		panel4.setPreferredSize(new Dimension(410, 50));
		tabbedPane.addTab("AG 4", null, panel4,
		                      "AG 4");
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);
		
		this.add(tabbedPane, BorderLayout.CENTER);*/
	}
	
	
	
	protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }
	public void insertarPlot(Plot2DPanel framePlot, String nombre) {
        //JPanel panel = new JPanel(false);
        //JLabel filler = new JLabel(nombre);
        //filler.setHorizontalAlignment(JLabel.CENTER);
        //panel.setLayout(new GridLayout(1, 1));
        //panel.add(framePlot);
        this.add(framePlot);
        this.etFuncion.setText(nombre);
		this.setBorder(BorderFactory.createTitledBorder(nombre));

    }

	public void quitarPlot(Plot2DPanel plot){
		this.removeAll();		
		init();

	}
	
	@Override
	public void addPlot(Plot2DPanel plot, String nombre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void minmax(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activarNumGenes(boolean equals) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarErrorDatos() {
		//mostramos un error de datos no validos
		this.panelError.setVisible(true);
	}

	
}

package gui;


import org.math.plot.Plot2DPanel;

public interface Observador {

	void addPlot(Plot2DPanel plot, String nombre);

	void removePlot(Plot2DPanel plot);

	void minmax(boolean b);

	void activarNumGenes(boolean equals);

	void mostrarErrorDatos();
	
}

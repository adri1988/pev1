package gui;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

import org.math.plot.Plot2DPanel;

import algoritmoGenetico.Elitismo;
import algoritmoGenetico.Funciones;
import algoritmoGenetico.Mutacion;
import algoritmoGenetico.NumGenes;
import algoritmoGenetico.Selecciones;
import algoritmoGenetico.TipoCruce;

public class PanelCampos extends JPanel implements Observador{
	private Controlador control;
	private int minMAX;
	
	private JPanel pCampos;
	private JPanel pBotones;
	
	//Campos:
	private JLabel etFuncion;
	private JComboBox<Object> boxFuncion;
	private JLabel etPrecision;
	private JTextField textPrecision;
	private JLabel etPoblacion;
	private JTextField textPoblacion;
	private JLabel etIter;
	private JTextField textIter;
	private JLabel etCruces;
	private JTextField textCruces;
	private JLabel etMutacion;
	private JTextField textMutacion;
	private JLabel etSemilla;
	private JTextField textSemilla;
	private JLabel etTipoCruce;
	private JComboBox<Object> boxTipoCruce;
	private JLabel etSelec;
	private JComboBox<Object> boxSelec;
	private JLabel etTipoMuta;
	private JComboBox<Object> boxTipoMuta;
	private JLabel etElite;
	private JComboBox<Object> boxElite;
	private JLabel etNumGenes;
	private JComboBox<Object> boxNumGenes;

	
	//Botones:
	private JButton lanza;
	private JButton relanza;
	private JButton elimina;
	
	public PanelCampos(Controlador c){
		control = c;
		control.addObserver(this);
		minMAX=1;
		init();
	}
	
	private void init(){
		setLayout(new BorderLayout());
		
		panelC();
		add(pCampos, BorderLayout.NORTH);

		panelB();
		add(pBotones, BorderLayout.SOUTH);
		eventos();
		this.setBorder(BorderFactory.createTitledBorder("Campos"));
	}


	private void panelC() {
		pCampos = new JPanel();
		pCampos.setLayout(new GridLayout(3,6));
		
		etFuncion=new JLabel("Funcion: ");
		etFuncion.setHorizontalAlignment(SwingConstants.RIGHT);
		//texalign right
		boxFuncion=new JComboBox(Funciones.values());
		etPrecision=new JLabel("Tolerancia: ");
		etPrecision.setHorizontalAlignment(SwingConstants.RIGHT);
		textPrecision=new JTextField(6);
		textPrecision.setText("0.0001");
		etPoblacion=new JLabel("Poblacion: ");
		etPoblacion.setHorizontalAlignment(SwingConstants.RIGHT);
		textPoblacion=new JTextField(6);
		textPoblacion.setText("20");
		etIter=new JLabel("Generaciones: ");
		etIter.setHorizontalAlignment(SwingConstants.RIGHT);
		textIter=new JTextField(6);
		textIter.setText("100");
		etCruces=new JLabel("Probabilidad de cruce: ");
		etCruces.setHorizontalAlignment(SwingConstants.RIGHT);
		textCruces=new JTextField(6);
		textCruces.setText("0.3");
		etTipoCruce=new JLabel("Reproduccion (Tipo de cruce): ");
		etTipoCruce.setHorizontalAlignment(SwingConstants.RIGHT);
		boxTipoCruce=new JComboBox(TipoCruce.values());
		etMutacion=new JLabel("Probabilidad de Mutacion: ");
		etMutacion.setHorizontalAlignment(SwingConstants.RIGHT);
		textMutacion=new JTextField(6);
		textMutacion.setText("0.01");
		etTipoMuta=new JLabel("Tipo de Mutacion: ");
		etTipoMuta.setHorizontalAlignment(SwingConstants.RIGHT);
		boxTipoMuta=new JComboBox(Mutacion.values());
		etSelec= new JLabel("Seleccion: ");
		etSelec.setHorizontalAlignment(SwingConstants.RIGHT);
		boxSelec= new JComboBox(Selecciones.values());
		etElite=new JLabel("Elitismo: ");
		etElite.setHorizontalAlignment(SwingConstants.RIGHT);
		boxElite=new JComboBox(Elitismo.values()); 
		etSemilla=new JLabel("Semilla: ");
		etSemilla.setHorizontalAlignment(SwingConstants.RIGHT);
		textSemilla=new JTextField(6);
		etNumGenes=new JLabel("Numero de genes: ");
		etNumGenes.setHorizontalAlignment(SwingConstants.RIGHT);
		boxNumGenes=new JComboBox(NumGenes.values());
		
		pCampos.add(etFuncion);
		pCampos.add(boxFuncion);
		pCampos.add(etPrecision);
		pCampos.add(textPrecision);
		pCampos.add(etPoblacion);
		pCampos.add(textPoblacion);
		pCampos.add(etIter);
		pCampos.add(textIter);
		pCampos.add(etCruces);
		pCampos.add(textCruces);
		pCampos.add(etTipoCruce);
		pCampos.add(boxTipoCruce);
		pCampos.add(etMutacion);
		pCampos.add(textMutacion);
		pCampos.add(etTipoMuta);
		pCampos.add(boxTipoMuta);
		pCampos.add(etSelec);
		pCampos.add(boxSelec);		
		pCampos.add(etElite);
		pCampos.add(boxElite);
		pCampos.add(etSemilla);
		pCampos.add(textSemilla);
		pCampos.add(etNumGenes);
		etNumGenes.setVisible(false);
		pCampos.add(boxNumGenes);
		boxNumGenes.setVisible(false);
	}
	
	private void panelB() {
		pBotones = new JPanel();
		
		lanza=new JButton("Lanzar AG");
		//relanza=new JButton("Relanzar AG");
		elimina=new JButton("Eliminar AG");
		
		pBotones.add(lanza);
		//pBotones.add(relanza);
		pBotones.add(elimina);
		
	}
	
	private void eventos(){
		lanza.addActionListener(new ActionListener() {
			

			public void actionPerformed(ActionEvent e) {
				double prec=Double.parseDouble(textPrecision.getText());
				double cruces=Double.parseDouble(textCruces.getText());
				int iter=Integer.parseInt(textIter.getText());
				int tam=Integer.parseInt(textPoblacion.getText());
				double muta=Double.parseDouble(textMutacion.getText());
				int funcion = boxFuncion.getSelectedIndex() + 1;
				int seleccion = boxSelec.getSelectedIndex() + 1;
				int repro = boxTipoCruce.getSelectedIndex() + 1;
				boolean elitismo; 
				if(boxElite.getSelectedIndex()==0) elitismo=false;
				else elitismo=true;
				int tipoMutacion = boxTipoMuta.getSelectedIndex();
				int ngenes = 0;
				if(boxFuncion.getSelectedItem().equals(Funciones.Funcion4))
					ngenes = boxNumGenes.getSelectedIndex() + 1; 
				
				if((prec>=0)&&(prec<=1)&&(cruces>=0)&&(cruces<=1)&&(muta>=0)&&(muta<=1)&&(iter>0)&&(tam>0)){
					control.algoritmo(funcion, ngenes, minMAX, tam, iter, prec, cruces, muta, seleccion, elitismo, repro, tipoMutacion);
				}else{
					control.datosErroneos();
				}
			}
		});
		boxFuncion.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Funciones f = (Funciones) boxFuncion.getSelectedItem();
				control.cambiarFuncion(f);
			}
		});
		elimina.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//quita el panel del plot
				control.eliminaPlot();
			}
		});
		
		
	}

	@Override
	public void addPlot(Plot2DPanel plot, String nombre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void minmax(boolean b) {
		if(b) this.minMAX=1;
		else this.minMAX=0;
	}

	@Override
	public void activarNumGenes(boolean equals) {
		etNumGenes.setVisible(equals);
		boxNumGenes.setVisible(equals);
	}

	@Override
	public void mostrarErrorDatos() {
		// TODO Auto-generated method stub
		
	}
	
	
	//Fin de clase
}

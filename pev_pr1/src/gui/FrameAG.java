package gui;


import javax.swing.*;

import org.math.plot.Plot2DPanel;

import java.awt.*;



public class FrameAG extends JFrame implements Observador{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Container panelPrincipal;
	private PanelCampos panelSup;
	private PanelGrafica panelInf;

	private Controlador control;
	

	public FrameAG(Controlador c) throws HeadlessException {
		super("Practica 1 - Programacion Evolutiva");
		this.control = c;
		this.panelPrincipal = this.getContentPane();
		control.addObserver(this);
		init();
	}
	
	private void init(){
		this.setLayout(new BorderLayout());
		
		//panelSup
		panelSup=new PanelCampos(this.control);
		//panelSup.setLayout(new GridLayout(3, 10));
		panelPrincipal.add(panelSup, BorderLayout.NORTH);
		
		//panelInf
		panelInf=new PanelGrafica(this.control);
		panelPrincipal.add(panelInf, BorderLayout.CENTER);
		
		this.setSize(1500,800);	
		this.setLocation(350, 100);
		this.setVisible(true);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void insertarPlot(Plot2DPanel plot, String nombre){
		//listaPlot.add(plot);
		panelInf.insertarPlot(plot, nombre);
		System.out.println("plot insertado");
	}
	public void quitarPlot(Plot2DPanel plot){
		panelInf.quitarPlot(plot);
		System.out.println("plot eliminado");
	}

	@Override
	public void addPlot(Plot2DPanel plot, String nombre) {
		this.insertarPlot(plot, nombre);
		/*this.setSize(600,600);*/
		this.setVisible(true);
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		this.quitarPlot(plot);
	}

	@Override
	public void minmax(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activarNumGenes(boolean equals) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarErrorDatos() {
		// TODO Auto-generated method stub
		
	}
	
}

package cromosoma;


public class cromosomaFuncion1 extends cromosoma{
	
	// extremos del intervalo considerado
	// para los valores del dominio
	int xmin;
	int xmax;	
	int longitudCromosoma;
	
	public cromosomaFuncion1(int Xmax, int Xmin){
		this.xmin=Xmin;
		this.xmax=Xmax;		
		this.aptitud=0;
		
	}
	
	public cromosomaFuncion1() {
		// TODO Auto-generated constructor stub
	}
	public cromosomaFuncion1(cromosomaFuncion1 hijo1) {
		this.aptitud = hijo1.getAptitud();
		this.fenotipo=hijo1.getFenotipo();
		this.longitudCromosoma=hijo1.getLongitudCromosoma();
		this.punt_acum=hijo1.punt_acum;
		this.xmax=hijo1.getXmax();
		this.xmin=hijo1.getXmin();
		setGenes(new boolean[longitudCromosoma]);
		for (int i=0;i<longitudCromosoma;i++){
			this.getGenes()[i]=hijo1.getGenes(i);
		}
	}

	public String toString(){
		String resultado = "";
		for (int i=0;i<longitudCromosoma;i++)
			resultado += getGenes()[i] +  " , ";
		return "Genes: " + resultado + " fenotipo: " + fenotipo + " Aptitud: " + aptitud + " puntuación: " + puntuacion + " Punt acumulada: " + punt_acum +'\n';
		
	}

	public int getXmin() {
		return xmin;
	}

	public void setXmin(int xmin) {
		this.xmin = xmin;
	}

	public int getXmax() {
		return xmax;
	}

	public void setXmax(int xmax) {
		this.xmax = xmax;
	}

	public int getLongitudCromosoma() {
		return longitudCromosoma;
	}

	public void setLongitudCromosoma(int longitudCromosoma) {
		this.longitudCromosoma = longitudCromosoma;
	}

	double fenotipo() {//el valor del individuo
		
		int bin = bin_dec(getGenes(), longitudCromosoma);		
		
		int valor = (int) Math.round (xmin + (xmax - xmin) * bin/ (Math.pow(2,longitudCromosoma) - 1) );
		//fenotipo = valor;
		return valor;
		
	} 
	
	double evalua() {//adaptacion del individuo
		
		double x; // fenotipo
		x = fenotipo();
		aptitud = (float) f(x);
		return aptitud;
		
	}
	
	private double f(double x) {
		double resultado = -Math.abs(x*Math.sin(Math.sqrt(Math.abs(x))));
		return resultado;
	}
	
	private int calculaLongitudCromosoma (int Xmax, int Xmin, double tol){
		
		int longitud;		
		longitud = (int) Math.round(Math.log(1+ (Math.abs(Xmax-Xmin)/tol)) / Math.log(2));		
		return longitud;
		
	}

	private int bin_dec(boolean[] genes, int longitudCromosoma2) {
		
		String binaryGen = "";
		
		for (int i=0;i<longitudCromosoma2;i++){   //Transformamos el array de boolan a string binario
			if (genes[i])
				binaryGen = binaryGen+'1' ;			
			else
				binaryGen = binaryGen+'0';	
		}
		
		int resultado = Integer.parseInt(binaryGen, 2);	
		return resultado;
	}
	
	@SuppressWarnings("unused")
	private int binarioADecimal(String numeroBinario){
		  
		  int longitud = numeroBinario.length();//Numero de digitos que tiene nuestro binario
		  int resultado = 0;//Aqui almacenaremos nuestra respuesta final
		  int potencia = longitud - 1;
		  for(int i = 0;i < longitud;i++){//recorremos la cadena de numeros
		   if(numeroBinario.charAt(i) == '1'){
		    resultado += Math.pow(2,potencia);
		   }
		   potencia --;//drecremantamos la potencia
		  }
		  return resultado;
		 }

	public void inicializaCromosoma(double tol) {
		longitudCromosoma= calculaLongitudCromosoma(xmax,xmin,tol);
		setGenes(new boolean[longitudCromosoma]);
		for (int i = 0; i <longitudCromosoma; i++) {
			
			double prob = Math.random();
			if (prob>=0.5)
				getGenes()[i]=true;
			else
				getGenes()[i]=false;
		}
	}

}
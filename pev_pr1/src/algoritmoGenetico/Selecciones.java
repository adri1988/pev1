package algoritmoGenetico;


public enum Selecciones {
	Ruleta("Ruleta"){},
	Torneo("Torneo deterministico"){},
	Truncamiento("Truncamiento"){},
	Estocastico("Estocastico universal"){}
	;
	
	private String desc;

	private Selecciones(String d){
		this.desc = d;
	}
	
	public String getDesc(){
		return desc;
	}
	
}

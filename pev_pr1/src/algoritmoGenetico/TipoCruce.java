package algoritmoGenetico;


public enum TipoCruce {
	Monopunto("Cruce monopunto o simple"){},
	Discreto("Cruce discreto uniforme"){},
	AritmeticoCompleto("Cruce aritmetico completo")
	;
	
	private String desc;

	private TipoCruce(String d){
		this.desc = d;
	}
	
	public String getDesc(){
		return desc;
	}
	
}

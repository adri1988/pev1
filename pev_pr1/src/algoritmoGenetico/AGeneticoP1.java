package algoritmoGenetico;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cromosoma.cromosoma;
import cromosoma.cromosomaFuncionGenerica;

public class AGeneticoP1
{

	@Override

	public String toString()
	{
		String resultado = "";
		for (int i = 0; i < tam_pob; i++)
			resultado += pob[i].toString();
		resultado = resultado + "El mejor: " + elMejor.toString() + "Posicion del mejor: " + pos_mejor + '\n' + '\n'
				+ '\n';
		return resultado;
	}

	cromosomaFuncionGenerica[] pob; // poblaciÃƒÂ³n
	int tam_pob; // tamaÃƒÂ±o poblaciÃƒÂ³n
	int num_max_gen; // nÃƒÂºmero mÃƒÂ¡ximo de generaciones
	cromosomaFuncionGenerica elMejor; // mejor individuo
	cromosoma[] elMejorLocal;
	int pos_mejor; // posiciÃƒÂ³n del mejor cromosoma
	double prob_cruce; // probabilidad de cruce
	double prob_mut; // probabilidad de mutaciÃƒÂ³n
	double tol; // tolerancia de la representaciÃƒÂ³n
	int gen_actual = 0;
	int tipoFuncion;
	ArrayList<cromosomaFuncionGenerica> elite;
	
	public AGeneticoP1()
	{
	}

	public void inicializa(int tam_pob2, int num_max_gen2, double tol2, double prob_cruce2, double prob_mutacion,
			int tipoFuncion, int n)
	{
		this.tam_pob = tam_pob2;
		this.num_max_gen = num_max_gen2;
		this.tol = tol2;
		this.prob_cruce = prob_cruce2;
		this.prob_mut = prob_mutacion;
		this.tipoFuncion = tipoFuncion;
		elMejor = new cromosomaFuncionGenerica(tipoFuncion, n);
		elMejorLocal = new cromosomaFuncionGenerica[num_max_gen + 1];
		pob = new cromosomaFuncionGenerica[tam_pob]; // creamos el Array de cromosomas
		for (int j = 0; j < tam_pob; j++)
		{
			pob[j] = new cromosomaFuncionGenerica(tipoFuncion, n);
			pob[j].inicializaCromosoma(tol);
			pob[j].setAptitud(pob[j].evalua());
		}
		gen_actual = 0;
		
	}

	public void evaluarPoblacionMIN()
	{// Evaluamos la poblaciÃ³n buscando el mÃ­nimo

		double punt_acu = 0; // puntuaciÃƒÂ³n acumulada
		double aptitud_mejor = 0; // mejor aptitud
		double sumaptitud = 0; // suma de la aptitud
		elMejorLocal[gen_actual] = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
		for (int i = 0; i < tam_pob; i++)
		{
			sumaptitud = sumaptitud + pob[i].getAptitud();
			if (pob[i].getAptitud() < aptitud_mejor)
			{
				pos_mejor = i;
				aptitud_mejor = pob[i].getAptitud();
				elMejorLocal[gen_actual].setAptitud(pob[i].getAptitud());
			}
		}

		for (int i = 0; i < tam_pob; i++)
		{

			pob[i].setPuntuacion(pob[i].getAptitud() / sumaptitud);
			pob[i].setPunt_acum(pob[i].getPuntuacion() + punt_acu);
			punt_acu = punt_acu + pob[i].getPuntuacion();
		}

		if (aptitud_mejor <= elMejor.getAptitud())
			elMejor = new cromosomaFuncionGenerica(pob[pos_mejor]);

		// elMejor.setAptitud(pob[pos_mejor].getAptitud());

	}

	public void evaluarPoblacionMAX()
	{// Evaluamos la poblaciÃ³n buscando el mÃ¡ximo

		double punt_acu = 0; // puntuaciÃƒÂ³n acumulada
		double aptitud_mejor = 0; // mejor aptitud
		double sumaptitud = 0; // suma de la aptitud
		elMejorLocal[gen_actual] = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
		for (int i = 0; i < tam_pob; i++)
		{
			sumaptitud = sumaptitud + pob[i].getAptitud();
			if (pob[i].getAptitud() > aptitud_mejor)
			{
				pos_mejor = i;
				aptitud_mejor = pob[i].getAptitud();
				elMejorLocal[gen_actual] = new cromosomaFuncionGenerica(pob[i]);

			}
		}

		for (int i = 0; i < tam_pob; i++)
		{

			pob[i].setPuntuacion(pob[i].getAptitud() / sumaptitud);
			pob[i].setPunt_acum(pob[i].getPuntuacion() + punt_acu);
			punt_acu = punt_acu + pob[i].getPuntuacion();
		}
		if (aptitud_mejor >= elMejor.getAptitud())
			// elMejor.setAptitud(pob[pos_mejor].getAptitud());
			elMejor = new cromosomaFuncionGenerica(pob[pos_mejor]);
	}

	public boolean terminado()
	{
		return gen_actual == num_max_gen;

	}

	public void seleccionRuleta()
	{
		int[] sel_super = new int[tam_pob];// seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posiciÃƒÂ³n del superviviente
		cromosomaFuncionGenerica[] nueva_pob = new cromosomaFuncionGenerica[tam_pob];
		for (int i = 0; i < tam_pob; i++)
		{
			prob = Math.random();
			pos_super = 0;
			while ((pos_super < tam_pob) && (prob > pob[pos_super].getPunt_acum()))
			{
				sel_super[i] = pos_super;
				pos_super++;
			}
		}
		for (int i = 0; i < tam_pob; i++)
		{// genero la poblaciÃ³n intermedia
			nueva_pob[i] = new cromosomaFuncionGenerica(pob[sel_super[i]]);
		}
		pob = nueva_pob;// actualizo la poblaciÃ³n
	}

	public void seleccionTruncamiento(int minMax)
	{

		int[] sel_super = new int[tam_pob];// seleccionados para sobrevivir
		int[] indices = new int[tam_pob];// indices
		cromosomaFuncionGenerica[] nueva_pob = new cromosomaFuncionGenerica[tam_pob];
		HashMap<Integer, Double> list = new HashMap<>();
		for (int i = 0; i < tam_pob; i++)
		{
			list.put(i, pob[i].getAptitud());
		}
		list = (HashMap<Integer, Double>) sortByValue(list);
		double trunc = 0.2;// usamos el 20% para el umbral
		int cantidadAptos = (int) (tam_pob * trunc);
		int repeticiones = tam_pob / cantidadAptos;

		Iterator it = list.entrySet().iterator();
		int p = 0;
		while (it.hasNext())
		{// Al salir de este bucle, en el array indices, voy a tener los indices de los
			// individuos ordenados de menor a mayor
			Map.Entry pair = (Map.Entry) it.next();
			int indice = (int) pair.getKey();
			indices[p] = indice;
			p++;
		}
		int desplazamiento = 0;
		for (int i = 0; i < cantidadAptos; i++)
		{
			int indice;
			if (minMax == 1)// si la funcion es de mÃ­nimos
				indice = indices[i];
			else// si es de mÃ¡ximos pillo el ultimo
				indice = indices[tam_pob - i - 1];

			for (int j = 0; j < repeticiones; j++)
			{
				sel_super[desplazamiento] = indice;
				desplazamiento++;
			}
		}

		// if (desplazamiento!=tam_pob)
		// rellenas restantes

		for (int i = 0; i < tam_pob; i++)
		{// genero la poblaciÃ³n intermedia
			nueva_pob[i] = new cromosomaFuncionGenerica(pob[sel_super[i]]);
		}
		pob = nueva_pob;// actualizo la poblaciÃ³n

	}

	private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map)
	{
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>()
		{
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
			{
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list)
		{
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	public void seleccionUniversalEstocastica()
	{
		/*
		 * Similar al muestreo proporcional pero ahora se genera un Ãºnico nÃºmero
		 * aleatorio simple r y a partir de Ã©l se calculan los restantes. ï�±Los
		 * individuos se mapean en segmentos continuos cuyo tamaÃ±o es el de su aptitud.
		 * ï�±Se colocan tantas marcas espaciadas por igual como individuos queremos
		 * seleccionar (N) ï�±La distancia entre las marcas es 1/N ï�±La posiciÃ³n de la
		 * primera marca se obtiene a partir de un nÃºmero aleatorio entre 0y 1/N. *
		 * 
		 */
		int[] sel_super = new int[tam_pob + 1];// seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posiciÃƒÂ³n del superviviente
		cromosomaFuncionGenerica[] nueva_pob = new cromosomaFuncionGenerica[tam_pob];
		double r = Math.random();// elemento aleatorio Ãºnico que voy a generar
		for (int i = 1; i <= tam_pob; i++)
		{// tengo que aplicar la formula Aj=(a+j-1)/k donde a es el valor aleatorio, j el
			// j-Ã©simo elemento y la K tam_pob
			pos_super = 0;
			prob = Math.random();
			float Ai = (float) ((r + i - 1) / tam_pob);// calculo la posiciÃ³n del i-Ã©simo elemento
			while ((prob > Ai) && (pos_super < tam_pob))
			{
				sel_super[i] = pos_super;
				pos_super++;
			}
		}
		for (int i = 0; i < tam_pob; i++)
		{// genero la poblaciÃ³n intermedia
			nueva_pob[i] = new cromosomaFuncionGenerica(pob[sel_super[i]]);
		}
		pob = nueva_pob;// actualizo la poblaciÃ³n

	}

	public void seleccionPorTruncamiento()
	{
		/*
		 * El mÃ©todo mÃ¡s elitista ï�±Los individuos se ordenan por su fitness. ï�±El
		 * parÃ¡metro utilizado es el umbral de truncamiento Trunc. ï�±Truncindica la
		 * proporciÃ³n de los vecinos a seleccionar como padres. VarÃ­a entre el 50% y
		 * el 10%. Estos se reproducen 1/p veces (1/0,5) o bien (1/0,1) ï�±De 100
		 * individuos, ï�®si trunc es el 50%, seleccionamos los 50 mejores y cada uno se
		 * selecciona 2 veces ï�®si trunc es el 10%, seleccionamos los 10 mejores y cada
		 * uno se selcciona 10 veces ï�±Los individuos por debajo del umbral no producen
		 * descendientes
		 */
	}

	public void seleccionPorTorneoDeterministicoMIN()
	{
		/*
		 * DeterminÃ­stica ï�®Cada elemento de la muestra se toma eligiendo el mejor de
		 * los individuos de un conjunto de zelementos(2 Ã³ 3) tomados al azar de la
		 * poblaciÃ³n base.
		 * 
		 * ï�®El proceso se repite kveces hasta completar la muestra.
		 * 
		 * ï�±ProbabilÃ­stica ï�®Se diferencia en el paso de selecciÃ³n del ganador del
		 * torneo. En vez de escoger siempre el mejor se genera un nÃºmero aleatorio del
		 * intervalo [0..1], si es mayor que un parÃ¡metro p (fijado para todo el
		 * proceso evolutivo) se escoge el individuo mÃ¡s alto y en caso contrario el
		 * menos apto. Generalmente p toma valores en el rango (0.5,1)
		 */
		int[] sel_super = new int[tam_pob];// seleccionados para sobrevivir
		cromosomaFuncionGenerica[] nueva_pob = new cromosomaFuncionGenerica[tam_pob];
		for (int i = 0; i < tam_pob; i++)
		{
			// tengo que obtener una muestra de tamaÃ±o 2 y asegurarme que no son iguales
			int muestra1 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			int muestra2 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			while (muestra1 == muestra2)
			{
				muestra1 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
				muestra2 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			}
			// Al salir del bucle me aseguro que muestra1!=Muestra2

			// Obtengo el fitness de cada muestra de la poblaciÃ³n
			double muestra1Fitness = pob[muestra1].getAptitud();
			double muestra2Fitness = pob[muestra2].getAptitud();

			// Las comparo
			int pos_super;// posiciÃ³n del superviviente
			if (muestra1Fitness < muestra2Fitness)
			{
				pos_super = muestra1;// es mejor la muestra 1
			} else
			{
				pos_super = muestra2;// es mejor la muestra 2
			}
			nueva_pob[i] = new cromosomaFuncionGenerica(pob[pos_super]);
		}

		pob = nueva_pob;// actualizo la poblaciÃ³n
	}

	public void seleccionPorTorneoDeterministicoMAX()
	{// en lugar de ser MIN y MAX, puedo tener un parÃ¡metro global que me diga si la
		// funciÃ³n es de mÃ­nimos o mÃ¡ximos y simplemente aÃ±adir un condicional en el
		// IF de los fitness
		/*
		 * DeterminÃ­stica ï�®Cada elemento de la muestra se toma eligiendo el mejor de
		 * los individuos de un conjunto de zelementos(2 Ã³ 3) tomados al azar de la
		 * poblaciÃ³n base.
		 * 
		 * ï�®El proceso se repite kveces hasta completar la muestra.
		 * 
		 * ï�±ProbabilÃ­stica ï�®Se diferencia en el paso de selecciÃ³n del ganador del
		 * torneo. En vez de escoger siempre el mejor se genera un nÃºmero aleatorio del
		 * intervalo [0..1], si es mayor que un parÃ¡metro p (fijado para todo el
		 * proceso evolutivo) se escoge el individuo mÃ¡s alto y en caso contrario el
		 * menos apto. Generalmente p toma valores en el rango (0.5,1)
		 */
		int[] sel_super = new int[tam_pob];// seleccionados para sobrevivir
		cromosomaFuncionGenerica[] nueva_pob = new cromosomaFuncionGenerica[tam_pob];
		for (int i = 0; i < tam_pob; i++)
		{
			// tengo que obtener una muestra de tamaÃ±o 2 y asegurarme que no son iguales
			int muestra1 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			int muestra2 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			while (muestra1 == muestra2)
			{
				muestra1 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
				muestra2 = randomWithRange(0, tam_pob - 1); // probabilidad de seleccion
			}
			// Al salir del bucle me aseguro que muestra1!=Muestra2

			// Obtengo el fitness de cada muestra de la poblaciÃ³n
			double muestra1Fitness = pob[muestra1].getAptitud();
			double muestra2Fitness = pob[muestra2].getAptitud();

			// Las comparo
			int pos_super;// posiciÃ³n del superviviente
			if (muestra1Fitness > muestra2Fitness)
			{
				pos_super = muestra1;// es mejor la muestra 1
			} else
			{
				pos_super = muestra2;// es mejor la muestra 2
			}
			nueva_pob[i] = new cromosomaFuncionGenerica(pob[pos_super]);
		}

		pob = nueva_pob;// actualizo la poblaciÃ³n
	}

	public void reproduccionDiscretaUniforme()
	{
		// seleccionados para reproducir
		int[] sel_cruce = new int[tam_pob];
		// contador seleccionados
		int num_sel_cruce = 0;
		double prob;

		// Se eligen los individuos a cruzar
		for (int i = 0; i < tam_pob; i++)
		{
			// se generan tam_pob nÃƒÂºmeros aleatorios en [0 1)
			prob = Math.random();
			// se eligen los individuos de las posiciones i si prob <prob_cruce
			if (prob < prob_cruce)
			{
				sel_cruce[num_sel_cruce] = i;
				num_sel_cruce++;
			}
		}

		// el numero de seleccionados se hace par

		if ((num_sel_cruce % 2) == 1)
			num_sel_cruce--;

		// se cruzan los individuos elegidos en un punto al azar

		for (int i = 0; i < num_sel_cruce; i = i + 2)
		{
			cromosomaFuncionGenerica hijo1 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			cromosomaFuncionGenerica hijo2 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			cruceDiscretoUniforme(pob[sel_cruce[i]], pob[sel_cruce[i + 1]], hijo1, hijo2);

			// los nuevos individuos sustituyen a sus progenitores
			pob[sel_cruce[i]] = hijo1;
			pob[sel_cruce[i + 1]] = hijo2;
		}

	}

	public void reproduccionSimple()
	{
		// seleccionados para reproducir
		int[] sel_cruce = new int[tam_pob];
		// contador seleccionados
		int num_sel_cruce = 0;
		;
		int punto_cruce;
		double prob;

		// Se eligen los individuos a cruzar
		for (int i = 0; i < tam_pob; i++)
		{
			// se generan tam_pob nÃƒÂºmeros aleatorios en [0 1)
			prob = Math.random();
			// se eligen los individuos de las posiciones i si prob <prob_cruce
			if (prob < prob_cruce)
			{
				sel_cruce[num_sel_cruce] = i;
				num_sel_cruce++;
			}
		}

		// el numero de seleccionados se hace par

		if ((num_sel_cruce % 2) == 1)
			num_sel_cruce--;

		// se cruzan los individuos elegidos en un punto al azar

		punto_cruce = randomWithRange(0, pob[0].getLongitudTotal());
		for (int i = 0; i < num_sel_cruce; i = i + 2)
		{
			cromosomaFuncionGenerica hijo1 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			cromosomaFuncionGenerica hijo2 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			// cruceSimple(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2,
			// punto_cruce);
			cruceSimple(pob[sel_cruce[i]], pob[sel_cruce[i + 1]], hijo1, hijo2, punto_cruce);

			// los nuevos individuos sustituyen a sus progenitores
			pob[sel_cruce[i]] = hijo1;
			pob[sel_cruce[i + 1]] = hijo2;
		}

	}

	public void reproduccionAritmeticaCompleta()
	{
		// seleccionados para reproducir
		int[] sel_cruce = new int[tam_pob];
		// contador seleccionados
		int num_sel_cruce = 0;
		double prob;

		// Se eligen los individuos a cruzar
		for (int i = 0; i < tam_pob; i++)
		{
			// se generan tam_pob nÃƒÂºmeros aleatorios en [0 1)
			prob = Math.random();
			// se eligen los individuos de las posiciones i si prob <prob_cruce
			if (prob < prob_cruce)
			{
				sel_cruce[num_sel_cruce] = i;
				num_sel_cruce++;
			}
		}

		// el numero de seleccionados se hace par

		if ((num_sel_cruce % 2) == 1)
			num_sel_cruce--;

		// se cruzan los individuos elegidos en un punto al azar

		for (int i = 0; i < num_sel_cruce; i = i + 2)
		{
			cromosomaFuncionGenerica hijo1 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			cromosomaFuncionGenerica hijo2 = new cromosomaFuncionGenerica(tipoFuncion, pob[0].getNumGenes());
			// cruceSimple(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2,
			// punto_cruce);
			cruceAritmeticoCompleto(pob[sel_cruce[i]], pob[sel_cruce[i + 1]], hijo1, hijo2);

			// los nuevos individuos sustituyen a sus progenitores
			pob[sel_cruce[i]] = hijo1;
			pob[sel_cruce[i + 1]] = hijo2;
		}

	}

	private void cruceSimple(cromosomaFuncionGenerica padre1, cromosomaFuncionGenerica padre2,
			cromosomaFuncionGenerica hijo1, cromosomaFuncionGenerica hijo2, int punto_cruce)
	{

		int i;
		hijo1.inicializaCromosoma(tol);
		hijo2.inicializaCromosoma(tol);
		// primera parte del intercambio: 1 a 1 y 2 a 2
		for (i = 0; i < punto_cruce; i++)
		{
			hijo1.getGenes()[i] = padre1.getGenes()[i];
			hijo2.getGenes()[i] = padre2.getGenes()[i];
		}
		for (i = punto_cruce; i < hijo1.getLongitudTotal(); i++)
		{
			hijo1.getGenes()[i] = padre2.getGenes()[i];
			hijo2.getGenes()[i] = padre1.getGenes()[i];
		}
		hijo1.evalua();
		hijo2.evalua();
	}

	private void cruceDiscretoUniforme(cromosomaFuncionGenerica padre1, cromosomaFuncionGenerica padre2,
			cromosomaFuncionGenerica hijo1, cromosomaFuncionGenerica hijo2)
	{

		hijo1.inicializaCromosoma(tol);
		hijo2.inicializaCromosoma(tol);
		// primera parte del intercambio: 1 a 1 y 2 a 2
		double pi = 0.4;
		/*
		 * for (int i=0;i<hijo1.getLongitudTotal();i++){ double probGenHijo =
		 * Math.random(); if (probGenHijo > pi){ //hijo 1 hereda del padre 1 e hijo 2
		 * hereda del padre 2 hijo1.genes[i] = padre1.genes[i]; hijo2.genes[i] =
		 * padre2.genes[i]; } else{ //hijo1 hereda del padre 2 e hijo 2 hereda del padre
		 * 1 hijo1.genes[i] = padre2.genes[i]; hijo2.genes[i] = padre1.genes[i];
		 * 
		 * } }
		 */

		int genes = padre1.getNumGenes();
		for (int i = 0; i < genes; i++)
		{
			double probGenHijo = Math.random();
			if (probGenHijo > pi)
			{
				// hijo 1 hereda del padre 1 e hijo 2 hereda del padre 2
				for (int j = 0; j < padre1.getLongitudCromosoma(i); j++)
				{
					hijo1.getGenes()[j] = padre1.getGenes()[j];
					hijo2.getGenes()[j] = padre2.getGenes()[j];
				}
			} else
			{
				// hijo1 hereda del padre 2 e hijo 2 hereda del padre 1
				for (int j = 0; j < padre1.getLongitudCromosoma(i); j++)
				{
					hijo1.getGenes()[j] = padre2.getGenes()[j];
					hijo2.getGenes()[j] = padre1.getGenes()[j];
				}
			}
		}

		hijo1.evalua();
		hijo2.evalua();

	}

	private void cruceAritmeticoCompleto(cromosomaFuncionGenerica padre1, cromosomaFuncionGenerica padre2,
			cromosomaFuncionGenerica hijo1, cromosomaFuncionGenerica hijo2)
	{

		/*
		 * Cruce aritmÃ©tico completo [whole arithmetic crossover] Variante mÃ¡s
		 * utilizada El primer hijo es: alfa*xi + (1-alfa)*yi El segundo hijo es:
		 * (1-alfa)*xi + alfa*yi
		 */
		double alfa = Math.random();
		hijo1.inicializaCromosoma(tol);
		hijo2.inicializaCromosoma(tol);

		int genes = padre1.getNumGenes();
		for (int i = 0; i < genes; i++)
		{

			double xi = padre1.getFenotipo(i);
			double yi = padre2.getFenotipo(i);
			double genHijo1 = alfa * xi + (1 - alfa) * yi;
			double genHijo2 = alfa * yi + (1 - alfa) * xi;

			/*
			 * hijo1.getFenotipo()[i]=genHijo1; hijo2.getFenotipo()[i]=genHijo2;
			 */
			double xMin = padre1.getxMin(i);
			double xMax = padre1.getxMax(i);
			int valorABinH1 = (int) ((-xMin + genHijo1)
					/ ((xMax - xMin) / Math.pow(2, padre1.getLongitudCromosoma(i))));
			int valorABinH2 = (int) ((-xMin + genHijo2)
					/ ((xMax - xMin) / Math.pow(2, padre1.getLongitudCromosoma(i))));
			double pru1 = 0, pru2 = 0;
			pru1 = xMin + (xMax - xMin) * valorABinH1 / (Math.pow(2, padre1.getLongitudCromosoma(i)) - 1);
			pru2 = xMin + (xMax - xMin) * valorABinH2 / (Math.pow(2, padre1.getLongitudCromosoma(i)) - 1);

			String binH1 = toBinary(valorABinH1);
			String binH2 = toBinary(valorABinH2);
			hijo1.setGen(i, binH1);
			hijo2.setGen(i, binH2);

		}
		hijo1.evalua();
		hijo2.evalua();

	}

	private String toBinary(int n)
	{
		String b = ""; // binary representation as a string
		while (n != 0)
		{
			int r = (int) (n % 2); // remainder
			b = r + b; // concatenate remainder
			n /= 2; // reduce n
		}
		return b;
	}

	public void mutacion()
	{
		boolean mutado;
		int i, j;
		double prob;
		for (i = 0; i < tam_pob; i++)
		{
			mutado = false;
			for (j = 0; j < pob[0].getLongitudTotal(); j++)
			{
				// se genera un numero aleatorio en [0 1)
				prob = Math.random();
				// mutan los genes con prob<prob_mut
				if (prob < prob_mut)
				{
					pob[i].getGenes()[j] = !(pob[i].getGenes()[j]);
					mutado = true;
				}
				if (mutado)
					pob[i].aptitud = pob[i].evalua();
			}
		}
	}

	int randomWithRange(int min, int max)
	{
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}

	public cromosomaFuncionGenerica[] getPob()
	{
		return pob;
	}

	public cromosomaFuncionGenerica getPob(int i)
	{
		return pob[i];
	}

	public void setPob(cromosoma[] pob)
	{
		this.pob = (cromosomaFuncionGenerica[]) pob;
	}

	public int getTam_pob()
	{
		return tam_pob;
	}

	public void setTam_pob(int tam_pob)
	{
		this.tam_pob = tam_pob;
	}

	public int getNum_max_gen()
	{
		return num_max_gen;
	}

	public void setNum_max_gen(int num_max_gen)
	{
		this.num_max_gen = num_max_gen;
	}

	public cromosomaFuncionGenerica getElMejor()
	{
		return elMejor;
	}

	public void setElMejor(cromosomaFuncionGenerica elMejor)
	{
		this.elMejor = elMejor;
	}

	public int getPos_mejor()
	{
		return pos_mejor;
	}

	public void setPos_mejor(int pos_mejor)
	{
		this.pos_mejor = pos_mejor;
	}

	public double getProb_cruce()
	{
		return prob_cruce;
	}

	public void setProb_cruce(double prob_cruce)
	{
		this.prob_cruce = prob_cruce;
	}

	public double getProb_mut()
	{
		return prob_mut;
	}

	public void setProb_mut(double prob_mut)
	{
		this.prob_mut = prob_mut;
	}

	public double getTol()
	{
		return tol;
	}

	public void setTol(double d)
	{
		this.tol = d;
	}

	public cromosoma[] getElMejorLocal()
	{
		return elMejorLocal;
	}

	public void setElMejorLocal(cromosoma[] elMejorLocal)
	{
		this.elMejorLocal = elMejorLocal;
	}

	public int getGen_actual()
	{
		return gen_actual;
	}

	public void setGen_actual(int gen_actual)
	{
		this.gen_actual = gen_actual;
	}

	public void setPob(cromosomaFuncionGenerica[] pob)
	{
		this.pob = pob;
	}

	public void separaMejores(int tamElite, int minMax)
	{
		ArrayList<cromosomaFuncionGenerica> auxiliar = new ArrayList<cromosomaFuncionGenerica>();
		elite = new ArrayList<cromosomaFuncionGenerica>();
		for (int i = 0; i < tam_pob; i++)
			auxiliar.add(pob[i]);
		for (int j = 0; j < tamElite; j++)
		{
			int mejor = sacaIndiceDelMejor(auxiliar, minMax);
			elite.add(auxiliar.get(mejor));
			auxiliar.remove(mejor);
			tam_pob--;
		}

		pob = (cromosomaFuncionGenerica[]) auxiliar.toArray(new cromosomaFuncionGenerica[tam_pob]);

	}

	private int sacaIndiceDelMejor(ArrayList<cromosomaFuncionGenerica> auxiliar, int minMax)
	{
		double mejor = auxiliar.get(0).getAptitud();
		int indiceMejor = 0;

		for (int i = 1; i < tam_pob; i++)
		{
			if (minMax == 1)
			{
				if (auxiliar.get(i).getAptitud() < mejor)
				{
					mejor = auxiliar.get(i).getAptitud();
					indiceMejor = i;
				}
			} else
			{
				if (auxiliar.get(i).getAptitud() > mejor)
				{
					mejor = auxiliar.get(i).getAptitud();
					indiceMejor = i;
				}
			}

		}
		return indiceMejor;
	}

	public void incluyeMejores(int tamElite)
	{
		cromosomaFuncionGenerica[] auxiliar = new cromosomaFuncionGenerica[tam_pob + tamElite];
		for (int i = 0; i < tam_pob; i++)
		{
			auxiliar[i] = new cromosomaFuncionGenerica(pob[i]);
		}
		for (int i = tam_pob; i < tamElite + tam_pob; i++)
		{
			auxiliar[i] = new cromosomaFuncionGenerica(elite.get(i - tam_pob));
		}
		pob = auxiliar;
		tam_pob = tam_pob + tamElite;

	}

}
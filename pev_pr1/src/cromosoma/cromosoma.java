package cromosoma;

public abstract class cromosoma {
	
	private boolean[] genes; //cadena de bits (genotipo)
	double[] fenotipo; //fenotipo
	int tipoFuncion;
	public boolean getGenes(int i) {
		return genes[i];
	}
	public void setGenes(boolean[] genes) {
		this.genes = genes;
	}
	public double[] getFenotipo() {
		return fenotipo;
	}
	public double getFenotipo(int i){
		return fenotipo[i];
	}
	public void setFenotipo(double[] fenotipo) {
		this.fenotipo = fenotipo;
	}	
	
	public int getTipoFuncion() {
		return tipoFuncion;
	}
	public void setTipoFuncion(int tipoFuncion) {
		this.tipoFuncion = tipoFuncion;
	}
	public double getAptitud() {
		return aptitud;
	}
	public void setAptitud(double aptitud) {
		this.aptitud = aptitud;
	}
	public double getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(double d) {
		this.puntuacion = d;
	}
	public double getPunt_acum() {
		return punt_acum;
	}
	public void setPunt_acum(double d) {
		this.punt_acum = d;
	}
	public boolean[] getGenes()
	{
		return genes;
	}
	public double aptitud;//función de evaluación fitness adaptación);
	double puntuacion; //puntuación relativa(aptitud/suma)
	double punt_acum; //puntuación acumulada para selección

}
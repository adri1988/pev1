package gui;


import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.math.plot.Plot2DPanel;

import algoritmoGenetico.AGeneticoP1;
import algoritmoGenetico.Funciones;





public class Controlador {
	
	private Funciones funcionActual;
	private ArrayList<Observador> observer;
	private Plot2DPanel plotActual;
	
	public Controlador(){
		funcionActual = Funciones.Funcion1;
		observer = new ArrayList<Observador>();
	}
	
	public void addObserver(Observador v) {
		this.observer.add(v);	
	}
	
	public void algoritmo(int tipoFuncion, int NumGenes, int minMax, int tam_pob,int num_max_gen, double tol,
							double prob_cruce, double prob_mutacion, int seleccion, boolean elitismo, 
							int reproduccion, int mutacion){
		
		double [] generaciones = new double[num_max_gen+1];		
		double [] aptitudesDelMejorGlobal = new double[num_max_gen+1];
		double [] aptitudesMejoresLocales = new double[num_max_gen+1];
		double [] mediasDeAptitudes = new double[num_max_gen+1];
		double [][] fenotipos;
		int tamElite=0;
		
		for (int i=0; i<=num_max_gen;i++){
			generaciones[i]=i; 
	     }  
		if (elitismo){
			 tamElite = (int) (tam_pob*0.02);
		}
		
		AGeneticoP1 p = new AGeneticoP1();
		p.inicializa(tam_pob,num_max_gen,tol,prob_cruce,prob_mutacion,tipoFuncion,NumGenes);//En todas las funciones el par�metro para el numeroo de genes va a ser -1 exceptoo para la funci�on 4
		evaluaPoblacionMinMax(p,minMax);
		int numGenes=p.getElMejor().getNumGenes();
		fenotipos = new double[num_max_gen+1][numGenes];
		mediasDeAptitudes[0]=calculaMediaDePoblacion(p);
		aptitudesDelMejorGlobal[0]=p.getElMejor().getAptitud();//Almaceno el mejor de la poblaci�on inicial	
		
		insertaFenotipos(fenotipos,p.getElMejor().getFenotipo(),numGenes,0);
	
		
		while (!p.terminado()){//Algoritmo gen�tico
			p.setGen_actual(p.getGen_actual() + 1);
			if(elitismo)
				p.separaMejores(tamElite,minMax);
			
			ejecutaSeleccionEnBaseATipoDeSeleccion(p,seleccion,minMax);
			ejecutaReproduccionEnBaseATipoDeReproduccion(p,reproduccion);			
			p.mutacion();
			if(elitismo)
				p.incluyeMejores(tamElite);
			evaluaPoblacionMinMax(p,minMax);
			mediasDeAptitudes[p.getGen_actual()]=calculaMediaDePoblacion(p);
			aptitudesDelMejorGlobal[p.getGen_actual()]=p.getElMejor().getAptitud();//Almaceno el mejor de la generaci�n actual			
			insertaFenotipos(fenotipos,p.getElMejor().getFenotipo(),numGenes,p.getGen_actual());
		}
		
	    calculaArrayDeAptitudesLocalesYDevuelveMedia(aptitudesMejoresLocales,p);
		
		
		
		Plot2DPanel plot = new Plot2DPanel();		
		plot.addLegend("SOUTH");
		plot.addLinePlot("Mejor Global", generaciones, aptitudesDelMejorGlobal);		
		plot.addLinePlot("Mejor Local", generaciones, aptitudesMejoresLocales);		
		plot.addLinePlot("Media de la Generaci�n", generaciones, mediasDeAptitudes);
		
		  JFrame f =muestraTabla(fenotipos,aptitudesDelMejorGlobal,p.getGen_actual(),numGenes);
		  f.setVisible(true);
		
		this.plotActual=plot;
		for(Observador o:observer){
			o.addPlot(plot, funcionActual.toString());
		}
		
	}

	private void insertaFenotipos(double[][] fenotipos, double[] ds, int numGenes, int fila) {
		for (int i=0;i<numGenes;i++){
			fenotipos[fila][i]=ds[i];			
		}
		
	}

	private static double calculaMediaDePoblacion(AGeneticoP1 p) {
		int tam=p.getTam_pob();
		double media =0;
		for (int i=0;i<tam;i++){
			media+=p.getPob(i).getAptitud();
		}
		media = media/tam;
		return media;
	}

	private static void evaluaPoblacionMinMax(AGeneticoP1 p, int minMax) {
		/*minMax=1 minimos
		 * minMax=0 maximos 
		 * */
		switch (minMax){
		case 1:p.evaluarPoblacionMIN();
			break;
		case 0:p.evaluarPoblacionMAX();
			break;
		default: break;
		}
		
	}

	private static float calculaArrayDeAptitudesLocalesYDevuelveMedia(double[] aptitudesMejoresLocales, AGeneticoP1 p) {
		float mediaAptitud=0;
		for (int i=0;i<=p.getNum_max_gen();i++){
			aptitudesMejoresLocales[i]=p.getElMejorLocal()[i].getAptitud();
			mediaAptitud +=aptitudesMejoresLocales[i];
		}
		mediaAptitud = mediaAptitud/(p.getNum_max_gen()+1);
		return mediaAptitud;
	}

	private static void ejecutaReproduccionEnBaseATipoDeReproduccion(AGeneticoP1 p, int reproduccion) {
		/*reproduccion=1 cruce simple
		 * Reproduccion=2 cruce discretoUniforme
		 * */
		switch (reproduccion){
		case 1:p.reproduccionSimple();
			break;
		case 2: p.reproduccionDiscretaUniforme();
			break;
		case 3: p.reproduccionAritmeticaCompleta();
		default: break;
		}
		
	}

	private static void ejecutaSeleccionEnBaseATipoDeSeleccion(AGeneticoP1 p, int seleccion,int minMax) {
		/*Seleccion = 1  Ruleta
		 * Seleccion=2   TorneoDeterministico
		 * Seleccion = 3 truncamiento
		 * Seleccion=4   Universal Estocastico
		 * 
		 * minMax = 1 si es de M�nimos o 0 si es de M�ximos
		 * */
		
		switch (seleccion){
		case 1:p.seleccionRuleta();
			break;
		case 2:
				if (minMax==1)
					p.seleccionPorTorneoDeterministicoMIN();
				if (minMax==0)
					p.seleccionPorTorneoDeterministicoMAX();
			break;
		case 3:
				p.seleccionTruncamiento(minMax);
			break;
		case 4:
				p.seleccionUniversalEstocastica();
			break;
		default: break;
		
		}
		
	}

	public void cambiarFuncion(Funciones f) {
		this.funcionActual = f;
		if(this.funcionActual.equals(Funciones.Funcion2) || this.funcionActual.equals(Funciones.Funcion3)){
			for(Observador o:observer){
				o.minmax(false);
				o.activarNumGenes(this.funcionActual.equals(Funciones.Funcion4));
			}
		}else{//es funcion 1, 4 o 5
			for(Observador o:observer){
				o.minmax(true);
				o.activarNumGenes(this.funcionActual.equals(Funciones.Funcion4));
			}
		}
		
	}

	public void eliminaPlot() {
		for(Observador o:observer){
			o.removePlot(plotActual);
		}
	}

	public void datosErroneos() {
		for(Observador o:observer){
			o.mostrarErrorDatos();
		}		
	}
	
	public static JFrame muestraTabla(double[][] fenotipos, double[] aptitudesDelMejorGlobal, int gen_actual, int numGenes){
	    JFrame f = new JFrame("Genes y aptitudes");
	    JTable tbl = new JTable(new CurrencyTableModel(fenotipos,aptitudesDelMejorGlobal,gen_actual,numGenes));

	   /* TableColumnModel tcm = tbl.getColumnModel();
	    tcm.getColumn(0).setPreferredWidth(150);
	    tcm.getColumn(0).setMinWidth(150);*/

	    tbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	   // tbl.setPreferredScrollableViewportSize(tbl.getPreferredSize());

	    JScrollPane sp = new JScrollPane(tbl);
	    f.getContentPane().add(sp, "Center");
	    f.pack();	    
	   return f;

}
	
}

class DataWithIcon {
	  public DataWithIcon(Object data, Icon icon) {
	    this.data = data;
	    this.icon = icon;
	  }

	  public Icon getIcon() {
	    return icon;
	  }

	  public Object getData() {
	    return data;
	  }

	  public String toString() {
	    return data.toString();
	  }

	  protected Icon icon;

	  protected Object data;
	}



	class CurrencyTableModel extends AbstractTableModel {
	  /**
		 * 
		 */
	  private static final long serialVersionUID = 1L;
	  protected String[] columnNames;// = { "Currency", "Yesterday", "Today", "Change","pepe" };
	  protected int filas;
	  protected int columnas;
	  protected double[][] grid;

	  // Constructor: calculate currency change to create the last column
	 /* public CurrencyTableModel() {
	    for (int i = 0; i < data.length; i++) {
	      data[i][DIFF_COLUMN] = new Double(((Double) data[i][NEW_RATE_COLUMN]).doubleValue()- ((Double) data[i][OLD_RATE_COLUMN]).doubleValue());
	    }
	  }*/

	  public CurrencyTableModel(double[][] fenotipos, double[] aptitudesDelMejorGlobal, int gen_actual,int numGenes) {
		  filas = gen_actual;
		  columnas=numGenes+1;
		  columnNames = new String[columnas];
		  for (int i=0;i<columnas-1;i++)
			columnNames[i] = "Gen: "+i;
		  columnNames[numGenes] = "Aptitud";
		  
		  grid = new double[filas][columnas];
		  for (int i=0;i<filas;i++){
			  for (int j=0;j<columnas-1;j++)
				  grid[i][j]= fenotipos[i][j];
			  grid[i][columnas-1]= aptitudesDelMejorGlobal[i];
		  }
		  filas = gen_actual;
		  
		  
	}

	// Implementation of TableModel interface
	  public int getRowCount() {
	    return filas;
	  }

	  public int getColumnCount() {
	    return columnas;
	  }

	  public Object getValueAt(int row, int column) {
					  return grid[row][column];
		
	  }

	  public String getColumnName(int column) {
	    return columnNames[column];
	  }
	
	
	}

